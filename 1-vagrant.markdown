# Razvojno okruzenje u Vagrantu

## Lagano i portablino razvojno okruzenje

- Razvojno okruzenje isto kao produkcijsko (heroku lucid32, Ubuntu 10.04)
- Identicno razvojno okruzenje za sve clanove tima ("Kod mene radi")
- Konfiguracija kompletne masine se nalazi u jednoj datoteci
	Vagrant u istom direktoriju vm
- Novi clanovi tima za par minuta imaju potpuno funkcionalno razvojno okruzenje sa svim potrebnim alatima i odgovarajucim verzijama
	- build-essentials
	- rvm
	- ruby
	- postgres
	- redis
	- memcache
	- java
	- elasticsearch
	- imagemagic

	~~~
	$ vagrant box add base http://files.vagrantup.com/lucid32.box
	$ vagrant init
	$ vagrant up
	~~~

- Editovanje se i dalje moze raditi sa host masine koristeci editor/okruzenje po izboru
- Na vagrant masini se pokrecu rails server i testovi

### Konfiguracija

~~~
Vagrant.configure do |config|
  config.vm.synced_folder "app/", "/rails-apps/rails-projekat", :nfs => true
  config.vm.forward_port 3000, 3000
end
~~~

### Synced Folders
	Da bi omogucili da se aplikacija moze editovati sa host masine, a pokrece na vagrantu potrebno je sinhronizovati projektne direktorije.
	Defaultni pristup je koristenje opcije `synced folders`

#### Default sync
Po defaultu vagrant ce automatski sinhronizirati direktorij sa Vagrant datotokom na host masini sa /vagrant u virtuelnoj.

#### Konfiguracija za sync folders

~~~
config.vm.synced_folder "app/", "/rails-apps/rails-projekat"
~~~

** Nakon svake izmjene Vagrant konfiguracije `vagrant reload` **

#### Performanse

Defaultni sync se ubrzo pokazao kao presporo rjesenje. Najvise prilikom dobavljanja asseta u development modu.

#### NFS

~~~
config.vm.synced_folder "app/", "/rails-apps/rails-projekat", :nfs => true
~~~

- Instalirati `nfsd` paket na host masini
- Samo linux i Mac OS X

#### Port forwarding

~~~
config.vm.forward_port 3000, 3000
~~~

- Da bi rails server bio vidljiv sa host masine





