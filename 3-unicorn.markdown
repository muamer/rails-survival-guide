# Unicorn, konkurentnost Heroku dyno-a

- Single-threaded rails serveri kao sto su Thin, Webrick mogu obradjivati jedan zahtjev u jednom trenutku.
  Ostli zahtjevi idu u red cekanja dok se predhodni ne obradi.

- Da bi povecali nivo konkurentosti aplikacije, potrebno je dodati veci broj dyno-a

- Da bi omogucili obradu veceg broja zahtjeva paralelno, potrebno je koristiti konkurentne servere kao sto je Unicorn.
  Pokrece vise ruby procesa unutar jednog dyno-a (Forking). Ne korisiti multithreading.

- Rails 4 je thread safe i mogu se koristiti prednosti threadinga i konkurentnost na nivou threadova

- Memorijsko ogranicenje jednog dyno-a 512MB
  Ovo je ogranicenje koje definise koliko konkurentnih instanci aplikacije mozemo pokrenuti u okviru jednog dyno-a
  Standardne aplikacije bi trebale biti u mogucnost da pokrenut 3-4 instance u okviru jednog dyno-a

## Setup

### Gemfile

~~~
  gem 'unicorn'
~~~

## Procfile

~~~
  web: bundle exec unicorn -p $PORT -c ./config/unicorn.rb
~~~

## config/unicorn.rb

[Heroku article](https://devcenter.heroku.com/articles/rails-unicorn)
~~~
worker_processes 2 # broj procesa unutar dyno-a 
preload_app true   # brze pokretanje aplikacija

after_fork do |server, worker|
  ActiveRecord::Base.establish_connection # Nakon kreiranja novog procesa, konektuj se na bazu
end
~~~
