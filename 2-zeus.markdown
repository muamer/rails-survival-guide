# Zeus, ubrzavanje sporih rails komandi

## Generatori

~~~
$ rails g model User username:string
      ...

real	0m13.471s
~~~

## rake taskovi, izvrsavanje migracija

~~~
$ rake db:migrate
    ...

real	0m5.815s
~~~

## Pokretanje konzole

~~~
$ rails c
    ...

real	0m9.859s
~~~

## Izvrsavanje testova

~~~
$ bundle exec rake spec
Finished in 1.52 seconds
20 examples, 0 failures

Randomized with seed 48392

real	0m17.112s
~~~

### Pokretanje kroz rspec

Zaobilazimo rake

~~~
$ bundle exec rspec spec
Finished in 1.62 seconds
20 examples, 0 failures

Randomized with seed 25734


real	0m11.314s
~~~

Razlika `real` i `Finished in` vremena se javlja zato sto se svaki put pokrece rails server prije izvrsavanja testova.

## Zeus

Samo prvi put pokrece rails server i za svako naredno izvrsavanje testova koristi vec pokrenutu instancu.

~~~
$ gem install zeus
$ zeus start (u drugom tabu)
~~~

~~~
Available Commands: [waiting] [crashed] [ready]
zeus generate (alias: g)
zeus destroy (alias: d)
zeus dbconsole
zeus server (alias: s)
zeus rake
zeus console (alias: c)
zeus runner (alias: r)
zeus cucumber [run to see backtrace]
zeus test (alias: rspec, testrb)
~~~

### Generatori

~~~
$ zeus g model User username:string
    ...

real	0m2.227s
~~~

### rake taskovi, izvrsavanje migracija

~~~
$ zeus rake db:migrate
    ...

real	0m1.851s
~~~


### Izvrsavanje testova

~~~
$ zeus test spec
Finished in 1.47 seconds
20 examples, 0 failures

Randomized with seed 0


real	0m3.540s
~~~


~~~
command     standad     zeus
------------------------------
generate    13.471s     2.227s 
migrate     5.815s      1.851s  
rspec       11.314s     3.540s
------------------------------    
~~~

## Testing Setup

### Gemfile

~~~
group :test, :development do
  gem 'rspec-rails'
end

group :test do
  gem 'factory_girl_rails'
  gem 'capybara'
  gem 'rb-fsevent', '~> 0.9'
  gem 'guard-rspec'
  gem 'zeus'
end
~~~

~~~
$ bundle
$ rails g rspec:install
$ touch spec/fixtures.rb
$ guard init rspec
~~~

### spec_helper.rb

~~~
ENV["RAILS_ENV"] ||= 'test'
require File.expand_path("../../config/environment", __FILE__)
require 'rspec/rails'

require 'capybara/rails'
require 'capybara/rspec'

Dir[Rails.root.join("spec/support/**/*.rb")].each {|f| require f}

RSpec.configure do |config|
  config.include Capybara::DSL, :type => :request
  config.fixture_path = "#{::Rails.root}/spec/fixtures"

  config.use_transactional_fixtures = true

  config.infer_base_class_for_anonymous_controllers = false

  config.order = "random"

  config.include FactoryGirl::Syntax::Methods

  # Focusing
  config.treat_symbols_as_metadata_keys_with_true_values = true
  config.filter_run focus: true
  config.run_all_when_everything_filtered = true
end
~~~

### Guardfile

~~~
guard 'rspec', all_on_start: false, all_after_pass: false, zeus: true, parallel: false, bundler: false do
  watch(%r{^spec/.+_spec\.rb$})
  watch(%r{^lib/(.+)\.rb$})     { |m| "spec/lib/#{m[1]}_spec.rb" }
  watch('spec/spec_helper.rb')  { "spec" }

  # Rails example
  watch(%r{^app/(.+)\.rb$})                           { |m| "spec/#{m[1]}_spec.rb" }
  watch(%r{^app/(.*)(\.erb|\.haml)$})                 { |m| "spec/#{m[1]}#{m[2]}_spec.rb" }
  watch(%r{^app/controllers/(.+)_(controller)\.rb$})  { |m| ["spec/routing/#{m[1]}_routing_spec.rb", "spec/#{m[2]}s/#{m[1]}_#{m[2]}_spec.rb", "spec/features/#{m[1]}_spec.rb"] }
  watch(%r{^spec/support/(.+)\.rb$})                  { "spec" }
  watch('config/routes.rb')                           { "spec/routing" }
  watch('app/controllers/application_controller.rb')  { "spec/controllers" }

  # Capybara features specs
  watch(%r{^app/views/(.+)/.*\.(erb|haml)$})          { |m| "spec/features/#{m[1]}_spec.rb" }
end
~~~

### Running guard

~~~
$ guard

# from Vagrant, guard is not supporting fs-event -p is for polling
$ guard -p 
~~~
