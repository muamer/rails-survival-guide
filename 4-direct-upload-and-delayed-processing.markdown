# Direct upload to s3

## Upload kroz dyno

- dyno je zauzet tokom kompletnog procesa uploada slike na s3
- tokom procesiranje zahtjeva se vrsi procesiranje slika
- Timeout heroku requesta je 30s

## Direct upload

- Uvodjenjem CORS-a Amazon je omogucio direktan upload slika, sa klijentske strane
- Jquery file upload plugin u novijim verzijama omogucava direktan upload na S3
  [Jquery File Direct Upload](https://github.com/blueimp/jQuery-File-Upload/wiki/Upload-directly-to-S3)
- Dalje procesiranje uploadovane slike se dalje moze vrsiti u backgrond jobu (Sidekiq)

## Sidekiq

- Sidekiq koristi threadove i u okviru jednog workera moze izvrsavati 25 pozadinskih poslova paralelno
  "What if 1 Sidekiq process could do the work of 20 Resque or DelayedJob processes?"

- Moguce definisati prioritete u redovima cekanja

~~~
#Procfile
worker: bundle exec sidekiq -q high,3 -q normal,2 -q default,1
~~~

~~~
# models/Image.rb

# race condition with ActiveRecord
after_commit :enqueue_job, :on => :create

def enqueue_job
  ImageWorker.perform_async(self.id)
end
~~~

~~~
# workers/ImageWorker.rb

class PostImageWorker
  include Sidekiq::Worker
  sidekiq_options :queue => :high, :retry => 4

  def perform(id)
    ...
  end
~~~
