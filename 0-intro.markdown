Set alata, konfuguracija, trikova koji ce olaksati svakodnevni rad u railsu. 
Na kraju svakog dijela se nalazi kompletna lista konfiguracijskih fileova i objesnjenja.

- Vagrant, postavka portabilne, lagane i brze virtuelne masine za pokretanje rails aplikacija.
- Zeus, ubrzavanje sporih rails komandi, koje koristimo svaki dan svaki dan.
- Povecavanje konkurentnosti Heroku dyno-a koristeci Unicorn
  - Memory usage
- Procesiranje slika na Heroku-u
  - client side upload slka na Amazon S3
  - Odgodjeno procesiranje koristeci sidekiq, direct carrierwave
